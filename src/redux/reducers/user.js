import {createSlice} from "@reduxjs/toolkit";


const userSlice = createSlice({
    name: 'user',
    initialState:{
        user: {
            email: ''
        }
    },
    reducers: {
        loginAccount : (state, action) => {
            state.user = action.payload
        },
        logoutAccount : (state, action) => {
            state.user = {
                email: ''
            }
        }
    }
})


export const {loginAccount, logoutAccount} = userSlice.actions
export default userSlice.reducer