import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import clientApi from "../../utils/axios";

export const getMovie = createAsyncThunk(
    'movies/getMovie',
    async (filter, {rejectWithValue}) => {
        try {
            const getMovieList = await clientApi(`movie?page=1&limit=10`)
            if (getMovieList.statusText !== 'OK') {
                throw new Error('Server error !')
            }
            return getMovieList.data
        } catch (err) {
            return  rejectWithValue(err.message)
        }
    }
)




const moviesSlice = createSlice({
    name: 'movie',
    initialState: {
        data: [],
        dataLength: 0,
        filter: {
            genre: '',
            year: '',
            search: ''
        },
        status: '',
        error: ''
    },
    reducers: {
        getMovie: (state, action) => {
            state.data = action.payload
            state.dataLength = action.payload.length
        },
        changeGenre: (state, action) => {
            state.filter.genre = action.payload
        },
        changeYear: (state, action) => {
            state.filter.year = action.payload
        },
        changeSearch: (state, action) => {
            state.filter.search = action.payload
        }
    },
    extraReducers: {
        [getMovie.pending] : (state,action) => {
            state.status = 'loading'
            state.error = ''
        },
        [getMovie.rejected] : (state,action) => {
            state.status = 'error'
            state.error = action.payload
        },
        [getMovie.fulfilled] : (state,action) => {
            state.status = 'resolve'
            state.error = ''
            state.data = action.payload
            state.dataLength = action.payload.length
        }
    }
})

export const {changeGenre, changeYear, changeSearch} = moviesSlice.actions
export default moviesSlice.reducer







