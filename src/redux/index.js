import {configureStore} from "@reduxjs/toolkit";
import movies from "./reducers/movies";
import user from "./reducers/user"


export const store = configureStore({
    reducer: {
        movies,
        user
    }
})