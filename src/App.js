import {Routes, Route} from 'react-router-dom'
import Home from "./pages/Home/Home";
import '../src/styles/style.scss'
import Watch from "./pages/Watch/Watch";
import Register from "./pages/Register/Register";
import Login from "./pages/Login/Login";
import Layout from "./Layout/Layout";
import Serials from "./pages/Serials/Serials";
import Movies from "./pages/Movies/Movies";
import RegisterPass from "./pages/Register/RegisterPass/RegisterPass";
import {useEffect} from "react";
import {useDispatch} from "react-redux";
import {loginAccount} from "./redux/reducers/user";
import Popular from "./pages/Popular/Popular";
import MyList from "./pages/MyList/MyList";

function App() {

    const dispatch = useDispatch()

    useEffect(() => {
        if (localStorage.getItem('user') !== null){
            dispatch(loginAccount(JSON.parse(localStorage.getItem('user'))))
        }
    }, [])

  return (
    <div className="App">
        <Routes>
            <Route path={'/'} element={<Layout/>}>
                <Route path={''} element={<Home/>}/>
                <Route path={'serials'} element={<Serials/>}/>
                <Route path={'movie'} element={<Movies/>}/>
                <Route path={'popular'} element={<Popular/>}/>
                <Route path={'mylist'} element={<MyList/>}/>
                <Route path={'watch'} element={<Watch/>}/>
            </Route>
            <Route path={'/login'} element={<Login/>}/>
            <Route path={'/register'} element={<Register/>}/>
            <Route path={'/registerPass'} element={<RegisterPass/>}/>
        </Routes>
    </div>
  );
}

export default App;
