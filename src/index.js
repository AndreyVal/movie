import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import {Provider} from "react-redux";
import {store} from "./redux";
import {BrowserRouter} from "react-router-dom";
import {SkeletonTheme} from "react-loading-skeleton";
import {Context} from "./utils/Context";


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(


    <SkeletonTheme baseColor="#222222" highlightColor="#444444">
        <Provider store={store}>
            <BrowserRouter>

                    <App />

            </BrowserRouter>
        </Provider>
    </SkeletonTheme>


);


