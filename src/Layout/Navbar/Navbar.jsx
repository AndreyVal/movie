import React, {useState} from 'react';
import '../../styles/navbar.scss'
import {IoMdNotifications} from 'react-icons/io'
import {AiOutlineDown} from 'react-icons/ai'
import {Link, NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {changeSearch} from "../../redux/reducers/movies";
import {logoutAccount} from "../../redux/reducers/user";



const Navbar = () => {
    const [isScrolled, setIsScrolled] = useState(false);
    const dispatch = useDispatch()
    const {user} = useSelector((store) => store.user)

    window.onscroll = () => {
        setIsScrolled(window.pageYOffset === 0 ? false : true);
        return () => (window.onscroll = null)
    }

    const handleChange = (e) => {
        dispatch(changeSearch(e.target.value))
    }

    return (
        <div className={isScrolled ? 'navbar scrolled' : 'navbar'}>
            <div className="container">
                <div className="navbar__left">
                    <Link to={'/'}>
                        <img className='navbar__logo' src="https://www.logo.wine/a/logo/Netflix/Netflix-Logo.wine.svg" alt="netflix"/>
                    </Link>

                    <span className='navbar__category'><NavLink className='navbar__link' to={'/'}>Главная</NavLink></span>
                    <span className='navbar__category'><NavLink className='navbar__link' to={'/serials'}>Сериалы</NavLink></span>
                    <span className='navbar__category'><NavLink className='navbar__link' to={'/movie'}>Фильмы</NavLink></span>
                    <span className='navbar__category'><NavLink className='navbar__link' to={'/popular'}>Популярное</NavLink></span>
                    <span className='navbar__category'><NavLink className='navbar__link' to={'/mylist'}>Избранное</NavLink></span>
                </div>

                <div className="navbar__right">
                    <form className='navbar__search-form'>
                        <input
                            className='navbar__search'
                            placeholder='Поиск'
                            type="search"
                            onChange={handleChange}/>
                    </form>

                    <IoMdNotifications size='24px'/>
                    {
                        user.email.length
                            ?
                            <div className="navbar__profile">
                            <img className='navbar__avatar' src="https://cdn-icons-png.flaticon.com/512/3135/3135715.png" alt=""/>
                                <AiOutlineDown/>
                                <div className="navbar__profile-options">
                                    <span className='navbar__profile-list'>Настройки</span>
                                    <p onClick={() => {
                                        dispatch(logoutAccount())
                                        localStorage.removeItem('user')
                                    }} className='navbar__profile-list'>Выйти</p>
                                </div>
                            </div>
                            :
                            <div className='navbar__profile'>
                                <Link className='navbar__profile-link' to={'/login'}>Логин</Link>
                                /
                                <Link className='navbar__profile-link' to={'/registerPass'}>Регистрация</Link>
                            </div>
                    }

                </div>
            </div>
        </div>
    );
};

export default Navbar;