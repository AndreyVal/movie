import React from 'react';
import {AiOutlineMail, AiOutlinePhone} from 'react-icons/ai'
import {FiFacebook, FiInstagram, FiTwitter, FiLinkedin} from 'react-icons/fi'
import {BiSupport} from 'react-icons/bi'
import {Link} from "react-router-dom";

const Footer = () => {
    return (
        <footer className='footer'>
            <div className="container">
                <div className="footer__row">
                    <ul className="footer__list">
                        <li className="footer__list-item"><Link className='footer__link title'>О нас</Link></li>
                        <li className="footer__list-item"><Link className='footer__link'>О компании</Link></li>
                        <li className="footer__list-item"><Link className='footer__link'>Вакансии</Link></li>
                        <li className="footer__list-item"><Link className='footer__link'>Информация для партнеров</Link></li>
                        <li className="footer__list-item"><Link className='footer__link'>Размещение рекламы</Link></li>
                        <li className="footer__list-item"><Link className='footer__link'>Пользовательское соглашение</Link></li>
                        <li className="footer__list-item"><Link className='footer__link'>Политика конфиденциальности</Link></li>
                    </ul>
                    <ul className="footer__list">
                        <li className="footer__list-item"><Link className='footer__link title'>Разделы</Link></li>
                        <li className="footer__list-item"><Link className='footer__link'>Фильмы</Link></li>
                        <li className="footer__list-item"><Link className='footer__link'>Сериалы</Link></li>
                        <li className="footer__list-item"><Link className='footer__link'>ТВ-шоу</Link></li>
                        <li className="footer__list-item"><Link className='footer__link'>Что посмотреть</Link></li>
                    </ul>
                    <ul className="footer__list">
                        <li className="footer__list-item"><Link className='footer__link title'>Служба поддержки</Link></li>
                        <button className="footer__btn">Написать в чат</button>
                        <div className="footer__block-btns">
                            <button className="footer__btns"><AiOutlineMail/></button>
                            <button className="footer__btns"><AiOutlinePhone/></button>
                            <button className="footer__btns"><BiSupport/></button>
                        </div>
                    </ul>
                    <div className="footer__list">
                        <span className='footer__title title'>Наши социальные сети</span>
                        <button className="footer__social-btns">
                            <button className="footer__btns"><FiLinkedin/></button>
                            <button className="footer__btns"><FiTwitter/></button>
                            <button className="footer__btns"><FiInstagram/></button>
                            <button className="footer__btns"><FiFacebook/></button>
                        </button>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;