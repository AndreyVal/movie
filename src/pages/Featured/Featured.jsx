import React from 'react';
import {FiPlay} from 'react-icons/fi'
import {AiOutlineInfoCircle} from 'react-icons/ai'
import {useState} from "react";
import Modal from "../Modal/Modal";

const Featured = ({type}) => {

    const [modalActive, setModalActive] = useState(false)

    return (
        <div className='featured'>
            <img className='featured__imgBg' src="https://kg-portal.ru/img/84857/main_2x.jpg" alt=""/>

            <div className="featured__info">
                <img className='featured__movie-logo' src="https://images.moviesanywhere.com/3aaa7596c5bf998408cdeefb2853b9f7/0b541edd-1bd5-46ae-8bc9-ba6de0dd4720.png" alt=""/>
                <span className='featured__desc'>
                    Оставшиеся в живых члены команды Мстителей и их союзники должны разработать новый план, который поможет противостоять разрушительным действиям могущественного титана Таноса. После наиболее масштабной и трагической битвы в истории они не могут допустить ошибку.
                </span>
                <div className="featured__buttons">
                    <button onClick={() => setModalActive(true)} className='featured__button play'>
                        <FiPlay size='24px'/>
                        <span className='featured__buttons-text'>Play</span>
                    </button>
                    <button className='featured__button more'>
                        <AiOutlineInfoCircle size='24px'/>
                        <span className='featured__buttons-text'>Info</span>
                    </button>
                </div>
            </div>
            <Modal active={modalActive} setActive={setModalActive}>
                <iframe width="100%" height="415" src="https://www.youtube.com/embed/hA6hldpSTF8"
                        title="YouTube video player" frameBorder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                        allowFullScreen></iframe>
            </Modal>
        </div>
    );
};

export default Featured;