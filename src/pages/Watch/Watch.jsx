import React from 'react';
import {BiArrowBack} from 'react-icons/bi'


const Watch = () => {
    return (
        <div className='watch'>
            <div className="watch__back">
                <BiArrowBack/>
                Home
            </div>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/ZsJz2TJAPjw"
                    title="YouTube video player" frameBorder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    allowFullScreen></iframe>
            {/*<video className='watch__video' autoPlay progress controls src="https://www.youtube.com/embed/ZsJz2TJAPjw"/>*/}
        </div>
    );
};

export default Watch;