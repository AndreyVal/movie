import React, {useEffect} from 'react';
import MoviesFilter from "./MoviesFilter/MoviesFilter";
import MoviesFilterYear from "./MoviesFilter/MoviesFilterYear";
import {useState} from "react";
import clientApi from "../../utils/axios";
import List from "../List/List";
import MoviesCard from "./MoviesCard/MoviesCard";
import MoviesList from "../Home/MoviesList/MoviesList";

const Movies = () => {


    const [filmData, setFilmData] = useState([])
    const [movies, setMovies] = useState([])
    const [value, setValue] = useState('')

    const filteredMovies = movies.filter(movie => {
        return movie.name.toLowerCase().includes(value.toLowerCase())
    })

    useEffect(() => {
        clientApi('movie?page=1&limit=30').then((res) => setFilmData(res.data.docs))
    }, [])

    return (
        <section className='movies'>
            <div className="container">
                <h2 className='movies__title serials'>Фильмы</h2>
                <div className="movies__filter">
                    <div className="movies__filter-sort">
                        <MoviesFilter/>
                        <MoviesFilterYear/>
                    </div>
                    <form className='movies__search-form'>
                        <input
                            className='movies__search'
                            placeholder='Поиск'
                            type="search"
                            onChange={(event) => setValue(event.target.value)}
                        />
                    </form>
                </div>
                <div className="movies__row">
                    {
                        filteredMovies.map((movie, index) => {
                            return (
                                <MoviesList movie={movie} key={index}/>
                            )
                        })
                    }
                    <MoviesList data={filmData}/>

                </div>
            </div>
        </section>
    );
};

export default Movies;