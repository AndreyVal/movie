import React from 'react';
import {BsFillPlayFill} from "react-icons/bs";
import {IoMdAdd} from "react-icons/io";
import {AiOutlineDislike, AiOutlineLike} from "react-icons/ai";

const MoviesCard = ({poster, genres, year, movieLength, description, name, id}) => {

    return (
        <div className="movies__card">
            <div className="movies__card-block">
                <img className='movies__card-block-img' src={poster.url} alt=""/>
                <div className="movies__card-info">
                    <h3 className="listItem__title">{name}</h3>
                    <div className="movies__card-info-time">
                        <span className="movies__card-rating">9.5</span>
                        <span>{Math.floor(movieLength / 60)} ч {movieLength % 60} мин</span>
                    </div>
                    <span>{year}г</span>
                    <div className="movies__card-icons">
                        <BsFillPlayFill  className='movies__card-icon'/>
                        <IoMdAdd  className='movies__card-icon'/>
                        <AiOutlineLike className='movies__card-icon'/>
                        <AiOutlineDislike className='movies__card-icon'/>
                    </div>
                    {
                        genres.map((g) => (<div className="movies__card-genre">{g.name}</div>))
                    }
                    {/*<div className="movies__card-desc">{description}</div>*/}
                </div>
            </div>
        </div>
    );
};

export default MoviesCard;