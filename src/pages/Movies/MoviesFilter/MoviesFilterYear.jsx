import React, {useEffect, useState} from 'react';
import {Box, FormControl, InputLabel, MenuItem, Select} from "@mui/material";
import {useDispatch} from "react-redux";
import {changeYear} from "../../../redux/reducers/movies";


const MoviesFilterYear = () => {
    const dispatch = useDispatch()
    const [year, setYear] = useState('');
    useEffect(() => {
        dispatch(changeYear(year))
    }, [year]);

    return (
        <Box className='movies__filter-box' sx={{minWidth: 120}}>
            <FormControl fullWidth>
                <InputLabel style={{color: 'white'}} id="demo-simple-select-label">Год</InputLabel>
                <Select style={{color: 'white'}}
                        labelId="demo-simple=select-label"
                        id="demo-simple-select"
                        value={year}
                        label="year"
                        onChange={(e) => setYear(e.target.value)}
                >
                    <MenuItem className='movies__filter-item' value="">По умолчанию</MenuItem>
                    <MenuItem className='movies__filter-item' value={2015}>2015</MenuItem>
                    <MenuItem className='movies__filter-item' value={2016}>2016</MenuItem>
                    <MenuItem className='movies__filter-item' value={2017}>2017</MenuItem>
                    <MenuItem className='movies__filter-item' value={2018}>2018</MenuItem>
                    <MenuItem className='movies__filter-item' value={2019}>2019</MenuItem>
                    <MenuItem className='movies__filter-item' value={2020}>2020</MenuItem>
                    <MenuItem className='movies__filter-item' value={2021}>2021</MenuItem>
                    <MenuItem className='movies__filter-item' value={2022}>2022</MenuItem>
                </Select>
            </FormControl>
        </Box>
    );
};

export default MoviesFilterYear;