import React, {useEffect, useState} from 'react';
import {Box, FormControl, InputLabel, MenuItem, Select} from "@mui/material";
import {useDispatch} from "react-redux";
import {changeGenre} from "../../../redux/reducers/movies";

const MoviesFilter = () => {

    const dispatch = useDispatch()
    const [genreState, setGenreState] = useState('')
    useEffect(() => {
        dispatch(changeGenre(genreState))
    }, [genreState])

    return (
        <Box className="movies__filter-box" sx={{minWidth: 250}}>
            <FormControl fullWidth>
                <InputLabel style={{color: 'white'}} id="demo-simple-select-label">Жанр</InputLabel>
                <Select style={{color: 'white'}}
                        labelId="demo-simple=select-label"
                        id="demo-simple-select"
                        value={genreState}
                        label="genre"
                        onChange={(e) => setGenreState(e.target.value)}
                >
                        <MenuItem className='movies__filter-item' value="">По умолчанию</MenuItem>
                        <MenuItem className='movies__filter-item' value="Приключения">Приключения</MenuItem>
                        <MenuItem className='movies__filter-item' value="Комедии">Комедии</MenuItem>
                        <MenuItem className='movies__filter-item' value="Фантастика">Фантастика</MenuItem>
                        <MenuItem className='movies__filter-item' value="Драма">Драма</MenuItem>
                        <MenuItem className='movies__filter-item' value="Документальный">Документальный</MenuItem>
                        <MenuItem className='movies__filter-item' value="Боевик">Боевик</MenuItem>
                        <MenuItem className='movies__filter-item' value="Ужасы">Ужасы</MenuItem>
                </Select>
            </FormControl>
        </Box>
    );
};

export default MoviesFilter;