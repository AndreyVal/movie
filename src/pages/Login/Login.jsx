import React, {useState} from 'react';
import {NavLink, useNavigate} from "react-router-dom";
import {useForm} from "react-hook-form";
import {useDispatch} from "react-redux";
import {authClient} from "../../utils/axios";
import {loginAccount} from "../../redux/reducers/user";


const Login = () => {

    const [show, setShow] = useState(false)
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const {
        register,
        reset,
        handleSubmit,
        formState: {
            errors
        }
    } = useForm({mode: "onBlur"})

    const onSubmit = (data) => {
        authClient.post('/login' , data)
            .then((resolve) => {
                dispatch(loginAccount({
                    ...resolve.data.user,
                    token: resolve.data.accessToken
                }))
                navigate('/')
                localStorage.setItem('user', JSON.stringify({
                    ...resolve.data.user,
                    token: resolve.data.accessToken
                }))
            }).catch((err) => alert(err))
    }

    return (
            <div className='login'>
                <div className="register__top">
                    <div className="register__top-wrapper">
                        <img className='register__top-logo' src="https://www.logo.wine/a/logo/Netflix/Netflix-Logo.wine.svg" alt=""/>

                    </div>
                </div>
                <div className="container">
                    <form onSubmit={handleSubmit(onSubmit)} noValidate className='login__form'>
                        <h1 className='login__form-title'>Войти</h1>
                        <div className="login__form-block">
                            <input {...register('email' , {
                                required: {
                                    message: 'Email обязательно к заполнению',
                                    value: true
                                },
                                pattern: {
                                    message: 'Напишите правильно свой email',
                                    value: /^[^ ]+@[^ ]+\.[a-z]{2,5}$/
                                }
                            })} className='login__form-input' type="email" placeholder='Адрес электронной почты'/>

                            <p className="registerPass__form-error">
                                {errors.email && errors.email?.message}
                            </p>
                        </div>

                        <div className="login__form-block">
                            <input {...register('password', {
                                required: {
                                    message: "Пароль обязателен к заполнению",
                                    value: true
                                },
                                pattern: {
                                    value: /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{7,}/g,
                                    message: "Пароль должен содержать не менее 7 символов, заглавную букву и число!"
                                }
                            })} className='login__form-input' type={show ? 'text' : 'password'} placeholder='Пароль'/>

                            <p className="registerPass__form-error">
                                {errors.password && errors.password?.message}
                            </p>
                        </div>


                        <label className="login__form-label">
                            <input checked={show} onChange={() => setShow(!show)} className='login__form-checkbox' type="checkbox"/>
                            <span className='login__form-show'>Показать пароль</span>
                        </label>

                        <button className="login__form-btn">Войти</button>
                        <span className='login__form-desc'>Впервые на Netflix? <b> <NavLink className='login__form-link' to={'/register'}>Зарегистрируйтесь сейчас.</NavLink> </b></span>
                        <small className='login__form-info'>Эта страница защищена Google reCAPTCHA, чтобы мы знали, что вы не бот. <b> Подробнее.</b></small>

                    </form>

                </div>
            </div>
    );
};

export default Login;