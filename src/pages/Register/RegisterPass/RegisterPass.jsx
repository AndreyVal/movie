import React from 'react';
import {NavLink, useNavigate} from "react-router-dom";
import {useState} from "react";
import {useForm} from "react-hook-form";
import {authClient} from "../../../utils/axios";
import {useDispatch} from "react-redux";
import {loginAccount} from "../../../redux/reducers/user";

const RegisterPass = () => {

    const [show, setShow] = useState(false)
    const dispatch = useDispatch()
    const navigate = useNavigate()


    const {
        register,
        reset,
        handleSubmit,
        formState: {
            errors
        }
    } = useForm({mode: "onBlur"})

    const onSubmit = (data) => {
        authClient.post('/register' , data)
            .then((resolve) => {
                dispatch(loginAccount({
                    ...resolve.data.user,
                    token: resolve.data.accessToken
                }))
                navigate('/')
                localStorage.setItem('user', JSON.stringify({
                    ...resolve.data.user,
                    token: resolve.data.accessToken
                }))
            }).catch((err) => alert(err))
    }

    return (
        <section className='registerPass'>
            <div className="registerPass__top">
                <div className="registerPass__top-wrapper">
                    <img className='registerPass__top-logo' src="https://www.logo.wine/a/logo/Netflix/Netflix-Logo.wine.svg" alt=""/>
                    <button className="registerPass__top-button"><NavLink className='register__top-link' to={'/login'}>Войти</NavLink></button>
                </div>
            </div>
            <div className="registerPass__block">
                <h1 className='registerPass__title'>Создайте пароль, чтобы <br/> начать использовать <br/> подписку</h1>

                <form onSubmit={handleSubmit(onSubmit)} noValidate className="registerPass__form">

                    <div className="registerPass__form-block">
                        <input {...register('email' , {
                            required: {
                                message: 'Email обязательно к заполнению',
                                value: true
                            },
                            pattern: {
                                message: 'Напишите правильно свой email',
                                value: /^[^ ]+@[^ ]+\.[a-z]{2,5}$/
                            }
                        })} className="registerPass__form-input" type="email" placeholder='Адрес электронной почты'/>
                        <p className="registerPass__form-error">
                            {errors.email && errors.email?.message}
                        </p>
                    </div>

                    <div className="registerPass__form-block">
                        <input {...register('password', {
                            required: {
                                message: "Пароль обязателен к заполнению",
                                value: true
                            },
                            pattern: {
                                value: /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{7,}/g,
                                message: "Пароль должен содержать не менее 7 символов, заглавную букву и число!"
                            }
                        })} className="registerPass__form-input" type={show ? 'text' : 'password'} placeholder='Введите пароль'/>
                        <p className="registerPass__form-error">
                            {errors.password && errors.password?.message}
                        </p>
                    </div>

                        <label className="registerPass__form-label">
                            <input checked={show} onChange={() => setShow(!show)} className='registerPass__form-checkbox' type="checkbox"/>
                            <span className='registerPass__form-show'>Показать пароль</span>
                        </label>
                        <button className='registerPass__form-btn'>Начать</button>
                    </form>
            </div>
        </section>
    );
};

export default RegisterPass;