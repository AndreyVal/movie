import React, {useRef, useState} from 'react';
import {useForm} from "react-hook-form";
import {NavLink} from "react-router-dom";


const Register = () => {

    // const [show, setShow] = useState(false)

    const {
        register,
        reset,
        handleSubmit,
        formState: {
            errors
        }
    } = useForm({mode: "onBlur"})

    const onSubmit = (data) => {
        console.log(data)
    }


    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const emailRef = useRef()
    const passwordRef = useRef()

    const handleStart = () => {
        setEmail(emailRef.current.value)
    }
    const handleFinish = () => {
        setPassword(passwordRef.current.value)
    }

    return (
        <div className='register'>
            <div className="register__top">
                <div className="register__top-wrapper">
                    <img className='register__top-logo' src="https://www.logo.wine/a/logo/Netflix/Netflix-Logo.wine.svg" alt=""/>
                    <button className="register__top-button"><NavLink className='register__top-link' to={'/login'}>Войти</NavLink></button>
                </div>
            </div>
            <div className="container">
              <h1 className='register__title'>Фильмы, сериалы и многое другое без ограничений.</h1>
                <h2 className='register__subtitle'>Смотрите где угодно. Отменить подписку можно в любое время.</h2>
                <p className='register__info'>Готовы смотреть? Введите адрес электронной почты, чтобы оформить или возобновить подписку.</p>

                <div className="register__form">
                    <input {...register('email' , {
                        required: {
                            message: '',
                            value: true
                        },
                        pattern: {
                            message: 'Напишите правильно свой email',
                            value: /^[^ ]+@[^ ]+\.[a-z]{2,5}$/
                        }
                    })} className="register__form-input" type="email" placeholder='Адрес электронной почты' ref={emailRef}/>
                    <p className="register__form-error">
                        {errors.email && errors.email?.message}
                    </p>
                    <button className='register__form-btn' onClick={handleStart}><NavLink className='goToPass' to={'/registerPass'}>Начать смотреть ></NavLink></button>
                </div>










                {/*{*/}
                {/*    !email ? (*/}
                {/*       */}
                {/*    ) : <form onSubmit={handleSubmit(onSubmit)} noValidate className="register__form">*/}
                {/*        <input {...register('password', {*/}
                {/*            required: {*/}
                {/*                message: "Пароль обязателен к заполнению",*/}
                {/*                value: true*/}
                {/*            },*/}
                {/*            pattern: {*/}
                {/*                value: /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{7,}/g,*/}
                {/*                message: "Пароль должен содержать не менее 7 символов, заглавную букву и число!"*/}
                {/*            }*/}
                {/*        })} className="register__form-input" type={show ? 'text' : 'password'} placeholder='Введите пароль' ref={passwordRef}/>*/}
                {/*        <p className="register__form-error">*/}
                {/*            {errors.password && errors.password?.message}*/}
                {/*        </p>*/}
                {/*        <label className="register__form-label">*/}
                {/*            <span className='register__form-show'>Показать</span>*/}
                {/*            <input checked={show} onChange={() => setShow(!show)} className='register__form-checkbox' type="checkbox"/>*/}
                {/*        </label>*/}
                {/*        <button className='register__form-btn' onClick={handleFinish}>Начать</button>*/}
                {/*    </form>*/}
                {/*}*/}

            </div>
        </div>
    );
};

export default Register;