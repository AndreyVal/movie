import React from 'react';
import {useEffect} from "react";
import clientApi from "../../utils/axios";
import {useState} from "react";

const Modal = ({active, setActive, children, id}) => {

    const [trailerUrl, setTrailerUrl] = useState(null)

    useEffect(() => {
        if (active === true){
            clientApi(`/movie/${id}`)
                .then(({data}) => setTrailerUrl(data.videos.trailers[0].url))
        }
    }, [id])

    return (
        <div className={active ? "modal active" : "modal"} onClick={() => setActive(false)}>
            <div className={active ? "modal__content active" : "modal__content"} onClick={e => e.stopPropagation()}>
                {children ?? <iframe width="100%" height="415" src={trailerUrl}
                                     title="YouTube video player" frameBorder="0"
                                     allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                     allowFullScreen></iframe>}
            </div>
        </div>
    );
};

export default Modal;