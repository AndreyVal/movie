import React, {useState} from 'react';
import '../../styles/home.scss'
import Featured from "../Featured/Featured";
import List from "../List/List";
import {useDispatch} from "react-redux";
import {useEffect} from "react";
import clientApi from "../../utils/axios";




const Home = () => {

    const [filmData, setFilmData] = useState([])
    const [serialData, setSerialData] = useState([])
    const [cartoonData, setCartoonData] = useState([])


    const dispatch = useDispatch()

    useEffect(() => {
        clientApi('movie?page=1&limit=10').then((res) => setFilmData(res.data.docs))
        clientApi('movie?page=1&limit=10&type=tv-series').then((res) => setSerialData(res.data.docs))
        clientApi('movie?page=1&limit=10&type=cartoon').then((res) => setCartoonData(res.data.docs))

    }, [])

    return (
        <div className='home'>
            <Featured/>
            <List data={filmData}/>
            <List data={serialData}/>
            <List data={cartoonData}/>
        </div>
    );
};

export default Home;