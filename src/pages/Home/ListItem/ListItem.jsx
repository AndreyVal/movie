import React from 'react';
import {BsFillPlayFill} from 'react-icons/bs'
import {IoMdAdd} from 'react-icons/io'
import {AiOutlineLike, AiOutlineDislike} from 'react-icons/ai'
import {useState} from "react";
import Modal from "../../Modal/Modal";


const ListItem = ({poster, genres, year, movieLength, description, name, id}) => {

    const [modalActive, setModalActive] = useState(false)

    const handleViewTrailer = () => {
        setModalActive(true)
    }

    return (
        <section  className='listItem'>
                <img className='listItem__img' src={poster.url} alt=""/>
                <h3 className="listItem__title">{name}</h3>
            <div className="listItem__info">
                <div className="listItem__icons">
                    <BsFillPlayFill  onClick={() => handleViewTrailer()}  className='listItem__icon'/>
                    <IoMdAdd  className='listItem__icon'/>
                    <AiOutlineLike className='listItem__icon'/>
                    <AiOutlineDislike className='listItem__icon'/>
                    <span className="listItem__rating">9.5</span>
                </div>
                <div className="listItem__info-top">
                    <span>{Math.floor(movieLength / 60)} ч {movieLength % 60} мин</span>
                    <span>{year}</span>
                </div>
                {
                    genres.map((g) => (<div className="listItem__genre">{g.name}</div>))
                }
                <div className="listItem__desc">{description}</div>
            </div>

            <Modal active={modalActive} setActive={setModalActive} id={id}>

            </Modal>
        </section>
    );
};

export default ListItem;