import React from 'react';
import MoviesCard from "../../Movies/MoviesCard/MoviesCard";



const MoviesList = ({data}) => {
    console.log(data)
    return (
        <section className="movies__list">
            <div className="container">
                <div className="movies__list-content">
                    {/*<h2 className="movies__list-title">*/}
                    {/*    */}
                    {/*</h2>*/}
                    <div className="movies__list-cards">
                        {
                            data?.map((movie) => {
                                return (<MoviesCard key={movie.id} {...movie}/>)
                            })
                        }
                    </div>

                </div>
            </div>
        </section>
    );
};

export default MoviesList;