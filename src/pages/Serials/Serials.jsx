import React, {useEffect} from 'react';

import MoviesFilter from "../Movies/MoviesFilter/MoviesFilter";
import MoviesFilterYear from "../Movies/MoviesFilter/MoviesFilterYear";
import {useState} from "react";
import clientApi from "../../utils/axios";
import MoviesList from "../Home/MoviesList/MoviesList";

const Serials = () => {

    const [serialData, setSerialData] = useState([])
    const [value, setValue] = useState('')

    const filteredMovies = serialData.filter(movie => {
        return movie.name.toLowerCase().includes(value.toLowerCase())
    })

    useEffect(() => {
        clientApi('movie?page=1&limit=30&type=tv-series').then((res) => setSerialData(res.data.docs))
    }, [])

    return (
        <section className='movies'>
            <div className="container">
                <h2 className='movies__title serials'>Сериалы</h2>
                <div className="movies__filter">
                    <div className="movies__filter-sort">
                        <MoviesFilter/>
                        <MoviesFilterYear/>
                    </div>
                    <form className='movies__search-form'>
                        <input
                            className='movies__search'
                            placeholder='Поиск'
                            type="search"
                            onChange={(event) => setValue(event.target.value)}
                        />
                    </form>
                </div>
                <div className="movies__row">
                    {
                        filteredMovies.map((movie, index) => {
                            return (
                                <MoviesList movie={movie} key={index}/>
                            )
                        })
                    }
                </div>
            </div>
        </section>
    );
};

export default Serials;