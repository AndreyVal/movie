import React, {useRef, useState} from 'react';
import {IoIosArrowBack, IoIosArrowForward} from 'react-icons/io'
import ListItem from "../Home/ListItem/ListItem";
import Modal from '../Modal/Modal'

const List = ({data}) => {

    const [isMoved, setIsMoved] = useState(false)
    const [sliderNumber, setSliderNumber] = useState(0)
    const [modalActive, setModalActive] = useState(false)

    const listRef = useRef()

    const handleClick = (direction) => {
        setIsMoved(true)
        let distance = listRef.current.getBoundingClientRect().x - 50
        if (direction === 'left' && sliderNumber > 0) {
            setSliderNumber(sliderNumber - 1)
            listRef.current.style.transform = `translateX(${230 + distance}px)`
        }
        if (direction === 'right' && sliderNumber < 5) {
            setSliderNumber(sliderNumber + 1)
            listRef.current.style.transform = `translateX(${-230 + distance}px)`
        }
    }

    console.log(data)
    return (
        <div className='list'>
            <div className="list__content">
                <span className="list__title">Фильмы</span>
                <div className="list__wrapper">
                    <IoIosArrowBack
                        className='list__slider-arrow left'
                        onClick={() => handleClick('left')}
                        style={{display: !isMoved && 'none'}}
                    />
                    <div onClick={() => setModalActive(true)} className="list__block">
                        <div  className="contain" ref={listRef}>
                            {
                                data?.map((movie) => {
                                    return (<ListItem key={movie.id} {...movie}/>)
                                })
                            }
                        </div>
                    </div>
                    <IoIosArrowForward className='list__slider-arrow right' onClick={() => handleClick('right')}/>
                </div>
            </div>

            {/*<Modal active={modalActive} setActive={setModalActive}>*/}

            {/*</Modal>*/}
        </div>
    );
};

export default List;