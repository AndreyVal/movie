import React from 'react';
import MoviesFilter from "../Movies/MoviesFilter/MoviesFilter";
import MoviesFilterYear from "../Movies/MoviesFilter/MoviesFilterYear";
import MoviesList from "../Home/MoviesList/MoviesList";
import {useEffect, useState} from "react";
import clientApi from "../../utils/axios";
import MoviesCard from "../Movies/MoviesCard/MoviesCard";

const Popular = () => {

    const [popularData, setPopularData] = useState([])
    const [movies, setMovies] = useState([])
    const [value, setValue] = useState('')

    const filteredMovies = movies.filter(movie => {
        return movie.name.toLowerCase().includes(value.toLowerCase())
    })

    useEffect(() => {
        clientApi('movie?page=1&limit=30').then((res) => setPopularData(res.data.docs))
    }, [])


    return (
        <section className='movies'>
            <div className="container">
                <h2 className='movies__title'>Популярное</h2>
                <div className="movies__filter">
                    <div className="movies__filter-sort">
                        <MoviesFilter/>
                        <MoviesFilterYear/>
                    </div>
                    <form className='movies__search-form'>
                        <input
                            className='movies__search'
                            placeholder='Поиск'
                            type="search"
                            onChange={(event) => setValue(event.target.value)}
                        />
                    </form>
                </div>
                <div className="movies__row">
                    {
                        filteredMovies.map((movie, index) => {
                            return (
                                <MoviesCard movie={movie} key={index}/>
                            )
                        })
                    }
                    <MoviesList data={popularData}/>
                </div>
            </div>
        </section>
    );
};

export default Popular;